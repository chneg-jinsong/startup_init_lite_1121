# Copyright (c) 2022 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import("//base/startup/init/begetd.gni")
import("//build/ohos.gni")

config("exported_header_files") {
  visibility = [ ":*" ]
  include_dirs = [
    "//base/startup/init/interfaces/innerkits/include",
    "//base/startup/init/services/include/param",
    "//base/startup/init/services/include",
    "//base/startup/init/services/loopevent/include",
    "//base/startup/init/services/log",
  ]
}

comm_sources = [
  "//base/startup/init/services/param/base/param_comm.c",
  "//base/startup/init/services/param/base/param_trie.c",
  "//base/startup/init/services/utils/init_hashmap.c",
  "//base/startup/init/services/utils/list.c",
]

base_include_dirs = [
  "//base/startup/init/services/param/include",
  "//base/startup/init/services/param/adapter",
  "//base/startup/init/services/param/linux",
  "//base/startup/init/services/param/base",
  "//third_party/bounds_checking_function/include",
]

if (defined(ohos_lite)) {
  static_library("parameterbase") {
    cflags = [ "-fPIC" ]
    sources = comm_sources
    sources += [
      "//base/startup/init/services/log/init_commlog.c",
      "//base/startup/init/services/param/base/param_base.c",
    ]
    include_dirs = base_include_dirs
    defines = [ "_GNU_SOURCE" ]
    public_configs = [ ":exported_header_files" ]

    if (ohos_kernel_type == "linux") {
      sources += [
        "//base/startup/init/services/param/adapter/param_dac.c",
        "//base/startup/init/services/param/linux/param_osadp.c",
      ]
      defines += [ "__LINUX__" ]
    } else if (ohos_kernel_type == "liteos_a") {
      sources += [
        "//base/startup/init/services/param/liteos/param_litedac.c",
        "//base/startup/init/services/param/liteos/param_osadp.c",
      ]
      defines += [
        "__LITEOS_A__",
        "WORKSPACE_AREA_NEED_MUTEX",
        "PARAM_PERSIST_SAVE_MUTEX",
        "PARAMWORKSPACE_NEED_MUTEX",
      ]
    }
    if (defined(config_ohos_startup_init_lite_memory_size)) {
      defines +=
          [ "PARAM_WORKSPACE_MAX=${config_ohos_startup_init_lite_memory_size}" ]
    }
  }
} else {
  if (!startup_init_with_param_base) {
    inherited_configs = [
      "//build/config/compiler:afdo",
      "//build/config/compiler:afdo_optimize_size",
      "//build/config/compiler:compiler",
      "//build/config/compiler:compiler_arm_fpu",
      "//build/config/compiler:compiler_arm_thumb",
      "//build/config/compiler:chromium_code",
      "//build/config/compiler:default_include_dirs",
      "//build/config/compiler:default_optimization",
      "//build/config/compiler:default_stack_frames",
      "//build/config/compiler:default_symbols",
      "//build/config/compiler:export_dynamic",
      "//build/config/compiler:no_exceptions",
      "//build/config/compiler:no_rtti",
      "//build/config/compiler:runtime_library",
      "//build/config/compiler:thin_archive",
      "//build/config/sanitizers:default_sanitizer_flags",
    ]
  }

  source_set("parameterbase") {
    sources = comm_sources
    sources += [
      "//base/startup/init/services/param/adapter/param_dac.c",
      "//base/startup/init/services/param/base/param_base.c",
      "//base/startup/init/services/param/linux/param_osadp.c",
    ]
    cflags = [ "-fPIC" ]
    include_dirs = base_include_dirs
    public_configs = [ ":exported_header_files" ]
    defines = [ "_GNU_SOURCE" ]
    deps = []

    if (startup_init_feature_decode_group_file) {
      defines += [ "PARAM_DECODE_GROUPID_FROM_FILE" ]
    }
    if (use_musl) {
      defines += [ "__MUSL__" ]
    }

    if (startup_init_test_performance) {
      defines += [ "PARAM_TEST_PERFORMANCE" ]
    }
    if (!startup_init_with_param_base) {
      ldflags = [ "-nostdlib" ]
      configs -= inherited_configs
      configs += [ "//build/config/compiler:compiler" ]
      defines += [ "PARAM_BASE" ]
    } else {
      include_dirs += [ "//third_party/bounds_checking_function/include" ]
      sources += [ "//base/startup/init/services/log/init_commlog.c" ]
    }
    if (build_selinux) {
      include_dirs += [
        "//third_party/selinux/libselinux/include/",
        "//base/security/selinux/interfaces/policycoreutils/include/",
      ]
      sources +=
          [ "//base/startup/init/services/param/adapter/param_selinux.c" ]
      defines += [
        "PARAM_SUPPORT_SELINUX",
        "PARAMWORKSPACE_NEED_MUTEX",
      ]
      deps += [ "//base/security/selinux:libselinux_parameter_static" ]
    }
    if (param_base_log) {
      defines += [ "PARAM_BASE_LOG" ]
    }
  }

  # extend for base
  ohos_static_library("parameterbase_ext") {
    sources = comm_sources
    sources += [
      "//base/startup/init/services/log/init_commlog.c",
      "//base/startup/init/services/param/linux/param_osadp.c",
    ]
    cflags = [ "-fPIC" ]
    include_dirs = base_include_dirs
    public_configs = [ ":exported_header_files" ]
    defines = [ "_GNU_SOURCE" ]
    deps = []

    include_dirs += [ "//third_party/bounds_checking_function/include" ]
    if (build_selinux) {
      include_dirs += [
        "//third_party/selinux/libselinux/include/",
        "//base/security/selinux/interfaces/policycoreutils/include/",
      ]
      defines += [
        "PARAM_SUPPORT_SELINUX",
        "PARAMWORKSPACE_NEED_MUTEX",
      ]
      deps += [ "//base/security/selinux:libselinux_parameter_static" ]
    }
    if (param_base_log) {
      defines += [ "PARAM_BASE_LOG" ]
    }
    part_name = "init"
    subsystem_name = "startup"
  }
}

group("param_base") {
  if (defined(ohos_lite)) {
    deps = [ ":parameterbase" ]
  } else {
    if (startup_init_with_param_base) {
      deps = [ ":parameterbase" ]
    } else {
      deps = [ ":parameterbase_ext" ]
    }
  }
}
